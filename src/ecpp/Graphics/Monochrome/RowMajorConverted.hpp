/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_GRAPHICS_MONOCHROME_ROWMAJORCONVERTED_HPP_
#define ECPP_GRAPHICS_MONOCHROME_ROWMAJORCONVERTED_HPP_

#include "ecpp/Graphics/Monochrome/PagedBufferProxy.hpp"
#include "ecpp/Graphics/Monochrome/PixelTransforms.hpp"
#include "ecpp/Meta/Indices.hpp"

namespace ecpp::Graphics::Monochrome
{

  template<int width_, int height_>
  class RowMajorConverted : public PagedBufferProxy<width_>
  {
  protected:
    typedef bool (*PixelTransform)(uint_least8_t x, uint_least8_t y, bool value);

    class OriginWrapper
    {
    public:
      static constexpr int kBitmapRowAdd = (width_ + 7) / (sizeof(uint8_t) * 8);
      static constexpr int kBitmapSize   = kBitmapRowAdd * height_;

      static constexpr bool GetPixel(const uint8_t (&row_major)[kBitmapSize], int col, int row, PixelTransform filter)
      {
        return filter(col, row, (row_major[(row * kBitmapRowAdd) + (col / 8)] & (0x01 << (col % 8))) != 0);
      }

      static constexpr uint8_t GetPage(const uint8_t (&row_major)[kBitmapSize], int col, int row, PixelTransform filter)
      {
        return (GetPixel(row_major,  col, row    , filter) ? 0x01 : 0) |
               (GetPixel(row_major,  col, row + 1, filter) ? 0x02 : 0) |
               (GetPixel(row_major,  col, row + 2, filter) ? 0x04 : 0) |
               (GetPixel(row_major,  col, row + 3, filter) ? 0x08 : 0) |
               (GetPixel(row_major,  col, row + 4, filter) ? 0x10 : 0) |
               (GetPixel(row_major,  col, row + 5, filter) ? 0x20 : 0) |
               (GetPixel(row_major,  col, row + 6, filter) ? 0x40 : 0) |
               (GetPixel(row_major,  col, row + 7, filter) ? 0x80 : 0);
      }
    };

    static constexpr int kPagedBufferCols  = width_;
    static constexpr int kPagedBufferPages = (height_ + 7) / 8;

    uint8_t buffer_[kPagedBufferPages * kPagedBufferCols];

    template<::ecpp::Meta::indices_type ...Is> constexpr 
    RowMajorConverted(const uint8_t (&row_major)[OriginWrapper::kBitmapSize], ::ecpp::Meta::indices<Is...>, PixelTransform filter) :
      PagedBufferProxy<width_>(buffer_, 0, 0, width_, height_),
      buffer_{OriginWrapper::GetPage(row_major, Is % kPagedBufferCols, Is / kPagedBufferCols * 8, filter)...} {}

  public:
    constexpr RowMajorConverted(const uint8_t (&row_major)[OriginWrapper::kBitmapSize], PixelTransform filter = PixelTransforms::Unchanged) :
      RowMajorConverted(row_major, ::ecpp::Meta::build_indices<sizeof(buffer_)>(), filter) {}

  };
}

#endif