/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_GRAPHICS_MONOCHROME_PAGEDBUFFERPROXY_HPP_
#define ECPP_GRAPHICS_MONOCHROME_PAGEDBUFFERPROXY_HPP_

#include "ecpp/Coordinates.hpp"
#include <cstdint>
#include <algorithm>

namespace ecpp::Graphics::Monochrome
{
  template<unsigned PageAdd, typename BufferElement = uint8_t>
  class PagedBufferProxy
  {
  public:
    using Point = ::ecpp::Point<uint8_t>;
    using Rect  = ::ecpp::Rect<Point>;

    constexpr PagedBufferProxy(BufferElement *buffer, uint_least8_t col, uint_least8_t row, uint_least8_t num_col, uint_least8_t num_row) :
      buffer_ptr_(buffer + row / 8 * PageAdd + col), row_offset_(row % 8), row_count_(num_row), col_count_(num_col) {}

    constexpr PagedBufferProxy(const PagedBufferProxy&) = default;

    constexpr PagedBufferProxy GetSubArea(Rect rect)
    {
      return GetSubAreaNoChecks(ClampToArea(rect));
    }

    constexpr Rect ClampToArea(Rect rect)
    {
      return { 
        ::std::min(col_count_, rect.upper_left.x),
        ::std::min(row_count_, rect.upper_left.y),
        ::std::min(col_count_, rect.lower_right.x),
        ::std::min(row_count_, rect.lower_right.y),
      };
    }

    constexpr PagedBufferProxy operator [] (Point p)
    {
      return GetSubArea({ p, {p.x+1, p.y+1}});
    }

    void     Clear();
    void     Invert();
    void     SetPixel(uint_least8_t col, uint_least8_t row);
    void     ClearPixel(uint_least8_t col, uint_least8_t row);

    const BufferElement & PageOf(uint_least8_t col, uint_least8_t row) const
    {
      return buffer_ptr_[(row_offset_ + row) / 8 * PageAdd + col];
    }

    BufferElement & PageOf(uint_least8_t col, uint_least8_t row)
    {
      return buffer_ptr_[(row_offset_ + row) / 8 * PageAdd + col];
    }

    class Calculator
    {
    public:
      constexpr Calculator(const PagedBufferProxy &proxy, uint_least8_t col, uint_least8_t row, uint8_t mask) :
        bits_lower_page{static_cast<uint_least8_t>((row + proxy.row_offset_) % 8)},
        bits_upper_page{static_cast<uint_least8_t>(8 - bits_lower_page)},
        mask_valid{static_cast<uint_least8_t>((proxy.row_count_ - row) > 7 ? 0xFF : 0xFF & (~(0xFF << (proxy.row_count_ - row))))},
        mask_upper{static_cast<uint_least8_t>(((mask & mask_valid) << bits_lower_page) & 0xFF)},
        mask_lower{static_cast<uint_least8_t>(((mask & mask_valid) >> bits_upper_page) & 0xFF)} {}

      uint_least8_t bits_lower_page;
      uint_least8_t bits_upper_page;

      uint_least8_t mask_valid;
      uint_least8_t mask_upper;
      uint_least8_t mask_lower;
    };

    void AssignCol(uint_least8_t col, uint_least8_t row, uint8_t value, uint8_t mask)
    {
      if( (row >= row_count_) || (col >= col_count_) )
        return;

      Calculator c(*this, col, row, mask);

      if(c.mask_upper)
        PageOf(col, row) = (PageOf(col, row) & ~c.mask_upper) | ((value << c.bits_lower_page) & c.mask_upper);

      if(c.mask_lower)
        PageOf(col, row + 8) = (PageOf(col, row + 8) & ~c.mask_lower) | ((value >> c.bits_upper_page) & c.mask_lower);
    }

    void OrCol(uint_least8_t col, uint_least8_t row, uint8_t value, uint8_t mask)
    {
      if( (row >= row_count_) || (col >= col_count_) )
        return;

      Calculator c(*this, col, row, mask);

      if(c.mask_upper)
        PageOf(col, row) |= ((value << c.bits_lower_page) & c.mask_upper);

      if(c.mask_lower)
        PageOf(col, row + 8) |= ((value >> c.bits_upper_page) & c.mask_lower);
    }

    uint8_t GetCol(uint_least8_t col, uint_least8_t row) const
    {
      if( (row >= row_count_) || (col >= col_count_) )
        return 0;

      Calculator c(*this, col, row, 0xFF);

      if(c.mask_lower == 0)
      {
        return (PageOf(col, row) >> c.bits_lower_page) & c.mask_valid;
      }
      else
      {
        return  ((PageOf(col, row)     >> c.bits_lower_page) | 
                 (PageOf(col, row + 8) << c.bits_upper_page)) & c.mask_valid;
      }
    }

    template<typename Source>
    void Copy(const Source& src, uint_least8_t col, uint_least8_t row);

    template<typename Source>
    void Or(const Source& src, uint_least8_t col, uint_least8_t row);

    constexpr auto GetWidth()  const { return col_count_; }
    constexpr auto GetHeight() const { return row_count_; }

    constexpr auto width()     const { return col_count_; }
    constexpr auto height()   const { return row_count_; }

  protected:
    constexpr PagedBufferProxy GetSubAreaNoChecks(Rect rect)
    {
      return {buffer_ptr_, 
        rect.upper_left.x, rect.upper_left.y,
        static_cast<uint_least8_t>(rect.lower_right.x - rect.upper_left.x),
        static_cast<uint_least8_t>(rect.lower_right.y - rect.upper_left.y),
      };
    }


    BufferElement  * const buffer_ptr_;
    const uint_least8_t    row_offset_;
    const uint_least8_t    row_count_;
    const uint_least8_t    col_count_;
  };

  template<unsigned PageAdd, typename BufferElement>
  void PagedBufferProxy<PageAdd, BufferElement>::Clear()
  {
    uint_least8_t x, y;

    for(y = 0; y < row_count_; y+=8)
      for(x = 0; x < col_count_; ++x)
        AssignCol(x, y, 0x00, 0xFF);
  }

  template<unsigned PageAdd, typename BufferElement>
  void PagedBufferProxy<PageAdd, BufferElement>::Invert()
  {
    uint_least8_t x, y;

    for(y = 0; y < row_count_; y+=8)
      for(x = 0; x < col_count_; ++x)
        AssignCol(x, y, ~GetCol(x,y), 0xFF);
  }

  template<unsigned PageAdd, typename BufferElement>
  void PagedBufferProxy<PageAdd, BufferElement>::SetPixel(uint_least8_t col, uint_least8_t row)
  {
    uint8_t mask;

    if( (row >= row_count_) || (col >= col_count_) )
      return;

    mask = (0x01 << (row + row_offset_) % 8);
    PageOf(col, row) |= mask;
  }

  template<unsigned PageAdd, typename BufferElement>
  void PagedBufferProxy<PageAdd, BufferElement>::ClearPixel(uint_least8_t col, uint_least8_t row)
  {
    uint8_t mask;

    if( (row >= row_count_) || (col >= col_count_) )
      return;

    mask = (0x01 << (row + row_offset_) % 8);
    PageOf(col, row) &= ~mask;
  }

  template<unsigned PageAdd, typename BufferElement>
  template<typename Source>
  void PagedBufferProxy<PageAdd, BufferElement>::Copy(const Source& src, uint_least8_t col, uint_least8_t row)
  {
    if( (row >= row_count_) || (col >= col_count_) )
      return;

    for(uint_least8_t r = 0; r < src.GetHeight(); ++r)
      for(uint_least8_t c = 0; c < src.GetWidth(); ++c)
        AssignCol(c+col, r+row, src.GetCol(c,r), ~(0xFF << (src.GetHeight() - r)));

  }

  template<unsigned PageAdd, typename BufferElement>
  template<typename Source>
  void PagedBufferProxy<PageAdd, BufferElement>::Or(const Source& src, uint_least8_t col, uint_least8_t row)
  {
    if( (row >= row_count_) || (col >= col_count_) )
      return;

    for(uint_least8_t r = 0; r < src.GetHeight(); ++r)
      for(uint_least8_t c = 0; c < src.GetWidth(); ++c)
        OrCol(c+col, r+row, src.GetCol(c,r), ~(0xFF << (src.GetHeight() - r)));

  }

}

#endif